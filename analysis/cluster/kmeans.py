import sys
import os
import numpy as np
import scipy
import pandas as pd

from sklearn.decomposition import PCA
from sklearn.decomposition import TruncatedSVD
from sklearn.cluster import KMeans
from sklearn.preprocessing import MinMaxScaler

from sklearn.metrics import silhouette_samples, silhouette_score
import argparse
from functools import reduce

import prince

import pickle
from scipy.io import savemat

import matplotlib 
from matplotlib import pyplot as plt

def load_mca_results(filterstatus):
    
    if filterstatus:
        with open("mca_outputs/filtered_mcares.pkl", 'rb') as resfile:
            res = pickle.load(resfile)
    else:
        with open("mca_outputs/mcares.pkl", 'rb') as resfile:
            res = pickle.load(resfile)
    X = res['X']
    ltcs = res['ltcs']
    filterstatus=res['filterstatus']
    mca_res = res['mca_res']
    return(X,ltcs,filterstatus,mca_res)

def run_kmeans(x, n_kmeans_clusters):
        
    kmeans = KMeans(n_clusters=n_kmeans_clusters,max_iter=10000, random_state = 0, n_init=500)
    kmeans.fit(x)
        
    return(kmeans)

filterstatus=True
X, ltcs, filterstatus, mca_res = load_mca_results(filterstatus=filterstatus)
print(np.shape(X))
change_mapping = {0:"no", 1:"yes"}
row_coords_mx = mca_res.row_coordinates(X.replace(change_mapping)).to_numpy()
    
#set n_mca_dims
n_mca_dims = len(np.where(mca_res.eigenvalues_ > np.mean(mca_res.eigenvalues_))[0])
print('retained', n_mca_dims, 'mca dimensions dims')
data_to_cluster = row_coords_mx[:,0:n_mca_dims]

for n_k_clusters in range(3,10):
    km = run_kmeans(data_to_cluster, n_k_clusters)
    fit_data = {'cluster_labels':km.labels_,'X':X}
    
    fname = 'kmeans/' + 'k'+str(n_k_clusters)+'_'+ str(filterstatus) + "filter.mat"
    print('saving samples and cluster labels to',fname)
    
    savemat(fname,fit_data)
    
filterstatus=False
X, ltcs, filterstatus, mca_res = load_mca_results(filterstatus=filterstatus)
print(np.shape(X))
change_mapping = {0:"no", 1:"yes"}
row_coords_mx = mca_res.row_coordinates(X.replace(change_mapping)).to_numpy()
    
#set n_mca_dims
n_mca_dims = len(np.where(mca_res.eigenvalues_ > np.mean(mca_res.eigenvalues_))[0])
print('retained', n_mca_dims, 'mca dimensions dims')
data_to_cluster = row_coords_mx[:,0:n_mca_dims]

for n_k_clusters in range(3,10):
    km = run_kmeans(data_to_cluster, n_k_clusters)
    fit_data = {'cluster_labels':km.labels_,'X':X}
    
    fname = 'kmeans/' + 'k'+str(n_k_clusters)+'_'+ str(filterstatus) + "filter.mat"
    print('saving samples and cluster labels to',fname)
    
    savemat(fname,fit_data)

